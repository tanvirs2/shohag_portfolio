<?php include ("html_inc/header.php");
$services_active = 1;
?>
	<body class="layout-body layout-body--services">
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="page-services">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">Services. <span class="layout-title__subtitle">What I offer</span></h1>
						</div>
						<div class="layout-row service-item">
							<div class="layout-container">
								<div class="service-item__icon-wrap">
									<div class="service-item__icon"><i class="fa fa-html5"></i></div>
								</div>
								<div class="service-item__content">
									<h2 class="service-item__title">Front-End Development</h2>
									<p class="service-item__description">I build customized cross-browser, cross-platform web sites and web applications. I use a wide array of the latest front end technologies, including: HTML5, CSS3, Preprocessors (LESS/SASS), etc.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="layout-row service-item">
							<div class="layout-container">
								<div class="service-item__icon-wrap">
									<div class="service-item__icon"><i class="fa fa-gears"></i></div>
								</div>
								<div class="service-item__content">
									<h2 class="service-item__title">Back-End Development</h2>
									<p class="service-item__description">I integrate your HTML and CSS templates into some of the most popular open source CMSs available (Wordpress, Yii, CakePHP, etc.) or develop a custom solution for your personal needs.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="layout-row service-item">
							<div class="layout-container">
								<div class="service-item__icon-wrap">
									<div class="service-item__icon"><i class="fa fa-paint-brush"></i></div>
								</div>
								<div class="service-item__content">
									<h2 class="service-item__title">Web Design</h2>
									<p class="service-item__description">I make design of layouts, design of web-pages, redesign of existing projects, responsive web-design, design of Wordpress themes and plugins, landing page design.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="layout-row service-item">
							<div class="layout-container">
								<div class="service-item__icon-wrap">
									<div class="service-item__icon"><i class="fa fa-search"></i></div>
								</div>
								<div class="service-item__content">
									<h2 class="service-item__title">Online Marketing</h2>
									<p class="service-item__description">Through effective online marketing strategies and my expert SEO services, I can help turn your website into your business' most powerful lead generator.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="layout-row service-item">
							<div class="layout-container">
								<div class="service-item__icon-wrap">
									<div class="service-item__icon"><i class="fa fa-camera"></i></div>
								</div>
								<div class="service-item__content">
									<h2 class="service-item__title">Photography</h2>
									<p class="service-item__description">Good quality photography is often the very first thing a prospective client or customer sees about your business. I can help you make a better first impression. I provide commercial photography services in Los Angeles.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="services.html#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="services.html#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="services.html#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="services.html#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="services.html#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="services.html#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:23 GMT -->
</html>
