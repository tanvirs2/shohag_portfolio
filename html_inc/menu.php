<?php


$menu_query = "SELECT * FROM `sidebar`";
$menu_result = mysqli_query($link, $menu_query);
$menu_row = mysqli_fetch_assoc($menu_result);
$expertises = $menu_row['info_1'];
$expertise_array = explode('_&_', $expertises);
?>

<script>!function(e,a,t,n,c,o,s){e.GoogleAnalyticsObject=c,e[c]=e[c]||function(){(e[c].q=e[c].q||[]).push(arguments)},e[c].l=1*new Date,o=a.createElement(t),s=a.getElementsByTagName(t)[0],o.async=1,o.src=n,s.parentNode.insertBefore(o,s)}(window,document,"script","../www.google-analytics.com/analytics.js","ga"),ga("create","UA-49600144-2","auto"),ga("send","pageview");</script>

		<div class="layout layout-width">
			<div class="layout-anti-scroll layout-anti-scroll--active layout-width"></div>
			<div class="layout-header layout-width">
				<header>
					<div id="headerMenuSlidingElement" class="main-nav__sliding-element"></div>
					<ul class="main-nav">
                                                <li class="main-nav__item <?php if (isset($index_active)){?> main-nav__item--active <?php } ?>"><a class="main-nav__item-link" href="index.php"><span class="main-nav__item-icon-wrapper"><i class="main-nav__item-icon icon-home"></i></span> <span class="main-nav__item-content">Home</span></a></li>
                                                <li class="main-nav__item <?php if (isset($resume_active)){?> main-nav__item--active <?php } ?>"><a class="main-nav__item-link" href="resume.php"><span class="main-nav__item-icon-wrapper"><i class="main-nav__item-icon icon-profile"></i></span> <span class="main-nav__item-content">Resume</span></a></li>
						
						<li class="main-nav__item <?php if (isset($portfolio_active)){?> main-nav__item--active <?php } ?>"><a class="main-nav__item-link" href="portfolio.php"><span class="main-nav__item-icon-wrapper"><i class="main-nav__item-icon icon-books"></i></span> <span class="main-nav__item-content">Portfolio</span></a></li>
						<li class="main-nav__item <?php if (isset($blog_active)){?> main-nav__item--active <?php } ?>"><a class="main-nav__item-link" href="blog.php"><span class="main-nav__item-icon-wrapper"><i class="main-nav__item-icon icon-blog"></i></span> <span class="main-nav__item-content">Tutorial</span></a></li>
						<li class="main-nav__item <?php if (isset($contact_active)){?> main-nav__item--active <?php } ?>"><a class="main-nav__item-link" href="contact.php"><span class="main-nav__item-icon-wrapper"><i class="main-nav__item-icon icon-envelop"></i></span> <span class="main-nav__item-content">Contact</span></a></li>
					</ul>
					<div class="mobile-menu"><a class="mobile-menu__toggle" ><span class="mobile-menu__toggle__icon"><i class="fa fa-bars mobile-menu__toggle__icon__open"></i> <i class="fa fa-times mobile-menu__toggle__icon__close"></i></span></a></div>
				</header>
			</div>
			<div class="layout-sidebar layout-sidebar-width">
				<div class="sidebar">
					<div class="sidebar__site-name"><?php echo $menu_row['name'];?></div>
					<div class="widget">
						<ul class="widget__list">
							<?php
							if(isset($expertise_array)){

								foreach ($expertise_array as $expertise){ ?>
									<li><?php echo $expertise;?></li>

								<?php }}?>
						</ul>
					</div>
					<ul class="social-icons">
						<li><a href="https://twitter.com/" target="_blank" class="social-icon--twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://facebook.com/" target="_blank" class="social-icon--facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://plus.google.com/" target="_blank" class="social-icon--google-plus"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="https://pinterest.com/" target="_blank" class="social-icon--pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="https://www.linkedin.com/" target="_blank" class="social-icon--linkedin"><i class="fa fa-linkedin"></i></a></li>
					</ul>

					<footer class="footer">
						<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
					</footer>
				</div>
			</div>