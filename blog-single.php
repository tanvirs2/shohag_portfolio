<?php include ("html_inc/header.php");?>
	<body class="layout-body layout-body--blog">
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="blog-single">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">blog. <span class="layout-title__subtitle">ABOUT EVERYTHING</span></h1>
						</div>
						<div class="layout-row"><img class="blog-single__image" src="uploads/blog/desktop.jpg" alt=""></div>
						<div class="blog-single__contents layout-horizontal-padding">
							<h2 class="blog-single__title">How Workspace Influences Productivity</h2>
							<ul class="blog-post__article-info">
								<li><a href="blog-single.php#"><i class="fa fa-calendar"></i>20 December 2014</a></li>
								<li><a href="blog-single.php#"><i class="fa fa-folder-open"></i>work</a></li>
								<li><a href="blog-single.php#"><i class="fa fa-tags"></i>work, office</a></li>
								<li><a href="blog-single.php#"><i class="fa fa-comments-o"></i>14 comments</a></li>
							</ul>
							<div class="blog-single__content">
								<p>Narwhal cornhole pop-up twee fixie. Normcore 90's quinoa ethical cardigan, crucifix gluten-free food truck Thundercats mustache. XOXO dreamcatcher beard, readymade hella messenger bag artisan Tumblr Helvetica Vice Pitchfork kogi health goth bitters tilde. Aesthetic taxidermy vegan paleo trust fund cliche. Viral bespoke Pinterest, mixtape Echo Park drinking vinegar fingerstache Marfa squid. Selfies selvage organic umami fashion axe listicle. Trust fund swag health goth, pop-up tousled pour-over 3 wolf moon mlkshk Vice small batch.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies erat at libero rhoncus, in condimentum erat volutpat. Nunc congue placerat lectus, in mollis metus scelerisque a. Integer venenatis faucibus enim nec dignissim.</p>
								<p>Donec dictum <a href="blog-single.php#">euismod augue at tempor.</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae laoreet arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse potenti. Donec eleifend lorem orci, ac scelerisque dui bibendum eget. Phasellus finibus elit eget eros ullamcorper volutpat. Nullam vel ultricies felis, a varius purus. Aliquam feugiat justo sit amet nulla pulvinar, vel cursus odio ultricies. Ut commodo ac quam vitae aliquam. Phasellus id purus arcu. Suspendisse dignissim ipsum sit amet magna finibus, non dictum nunc condimentum.</p>
								<p>Nunc laoreet interdum diam in hendrerit. Duis maximus vitae sapien quis blandit. Nullam risus tellus, tincidunt eget vulputate at, convallis sed ligula. Duis erat mi, gravida commodo mollis vitae, rutrum at sem. Vestibulum varius eleifend enim, quis consequat orci iaculis non. Aliquam dignissim elit sed varius laoreet. Integer ullamcorper auctor orci, venenatis fermentum leo euismod sed. Praesent varius nisl sit amet sagittis tempus. Sed gravida, mauris ut sagittis lacinia, metus lorem consectetur nunc, ac tristique orci neque in arcu. Suspendisse dui velit, sagittis et erat id, vestibulum luctus lectus. Nam ac eros porta, eleifend purus vitae, vehicula magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin ut ipsum bibendum, volutpat justo sed, dictum dui. Proin in hendrerit eros.</p>
							</div>
							<div class="layout-horizontal-padding">
								<div class="post-navigation">
									<div class="post-navigation__item-left"><a href="blog-single.php#" class="post-navigation__item-content"><i class="fa fa-arrow-circle-left post-navigation__icon"></i>Benefits of Being Well-Organized</a></div>
									<div class="post-navigation__item-right"><a href="blog-single.php#" class="post-navigation__item-content"><i class="fa fa-arrow-circle-right post-navigation__icon"></i>Do You Still Read Paper Books?</a></div>
								</div>
							</div>
							<div class="share-box">
								<h3 class="share-box__title">Share</h3>
								<div id="shareBoxContainer" class="share-box__items-contaner">
									<div class="share-box__item share-box__item--facebook" data-btntype="facebook" data-title="&nbsp;"></div>
									<div class="share-box__item share-box__item--twitter" data-btntype="twitter" data-title="&nbsp;"></div>
									<div class="share-box__item share-box__item--gplus" data-btntype="googlePlus" data-title="&nbsp;"></div>
									<div class="share-box__item share-box__item--pinterest" data-btntype="pinterest" data-title="&nbsp;"></div>
									<div class="share-box__item share-box__item--linkedin" data-btntype="linkedin" data-title="&nbsp;"></div>
								</div>
							</div>
							<div class="layout-horizontal-padding">
								<div class="layout-row about-author">
									<div class="layout-container">
										<h3 class="about-author__title">About author</h3>
										<div class="about-author__image-wrap"><img src="uploads/blog/about-author.jpg" alt=""></div>
										<div class="about-author__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
									</div>
								</div>
							</div>
							<div class="comments">
								<h3 class="comments__title">4 comments</h3>
								<div class="comments__items">
									<div class="comment-item">
										<div class="comment-item__info">
											<div class="comment-item__info__image-wrap"><img src="uploads/comments/marinella.jpg" alt=""></div>
											<div class="comment-item__info__name">Marinella Taranto</div>
											<div class="comment-item__info__date">28 December 2014</div>
											<div class="comment-item__info__reply"><a class="btn btn--style3" href="blog-single.php#">Reply</a></div>
										</div>
										<div class="comment-item__content">Nunc laoreet interdum diam in hendrerit. Duis maximus vitae sapien quis blandit. Nullam risus tellus, tincidunt eget vulputate at, convallis sed ligula. Duis erat mi, gravida commodo mollis vitae, rutrum at sem. Vestibulum varius eleifend enim, quis consequat orci iaculis non. Aliquam dignissim elit sed varius laoreet.</div>
									</div>
									<div class="comment-item">
										<div class="comment-item__info">
											<div class="comment-item__info__image-wrap"><img src="uploads/comments/ryan.jpg" alt=""></div>
											<div class="comment-item__info__name">Ryan Johnson</div>
											<div class="comment-item__info__date">26 December 2014</div>
											<div class="comment-item__info__reply"><a class="btn btn--style3" href="blog-single.php#">Reply</a></div>
										</div>
										<div class="comment-item__content">Aliquam interdum dapibus venenatis. Sed tempus suscipit imperdiet. Cras vitae lectus rutrum tortor euismod mattis. Etiam convallis lectus eu bibendum interdum. Etiam tempus justo nec nunc gravida, eget placerat arcu feugiat. Proin rutrum nulla sed aliquam sagittis. Duis vel tincidunt sapien.</div>
										<div class="comments__reply">
											<div class="comment-item">
												<div class="comment-item__info">
													<div class="comment-item__info__image-wrap"><img src="uploads/comments/marinella.jpg" alt=""></div>
													<div class="comment-item__info__name">Marinella Taranto</div>
													<div class="comment-item__info__date">26 December 2014</div>
													<div class="comment-item__info__reply"><a class="btn btn--style3" href="blog-single.php#">Reply</a></div>
												</div>
												<div class="comment-item__content">Nam ultrices neque arcu, et varius nisi tempor at. In hac habitasse platea dictumst.</div>
											</div>
										</div>
									</div>
									<div class="comment-item">
										<div class="comment-item__info">
											<div class="comment-item__info__image-wrap"><img src="uploads/comments/sarah.jpg" alt=""></div>
											<div class="comment-item__info__name">Sarah Brown</div>
											<div class="comment-item__info__date">10 December 2014</div>
											<div class="comment-item__info__reply"><a class="btn btn--style3" href="blog-single.php#">Reply</a></div>
										</div>
										<div class="comment-item__content">Maecenas facilisis accumsan eros in gravida. Ut elit dui, vehicula sed lacinia non, sodales non erat. Suspendisse mollis, ex ut rhoncus imperdiet, lacus ipsum lobortis tellus, in fermentum sem lorem quis libero. Ut at ex neque. Pellentesque at quam velit. Suspendisse eleifend vestibulum justo ac ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras tempor vehicula justo, nec laoreet massa vehicula in. Suspendisse pharetra interdum dui. Phasellus interdum nisl arcu, ac fringilla ipsum interdum ac.</div>
									</div>
								</div>
								<div class="comments__pagination">
									<ul class="comments__pagination__list">
										<li><a class="comments__pagination__list__item-current" href="blog-single.php#">1</a></li>
										<li><a href="blog-single.php#">2</a></li>
										<li><a href="blog-single.php#">3</a></li>
									</ul>
								</div>
								<div class="layout-horizontal-padding">
									<div class="comments__form">
										<h3 class="comments__form__title">leave a reply</h3>
										<form onsubmit="return!1">
											<div class="row">
												<div class="col-md-6"><input class="form__item" type="text" placeholder="name"></div>
												<div class="col-md-6"><input class="form__item" type="text" placeholder="email"></div>
											</div>
											<textarea class="form__textarea form__item" placeholder="message"></textarea>
											<button type="submit" class="btn btn-lg btn--style1">Post comment</button>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="layout-horizontal-padding--clean"></div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="blog-single.php#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="blog-single.php#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="blog-single.php#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="blog-single.php#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="blog-single.php#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="blog-single.php#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/blog-single.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:57 GMT -->
</html>
