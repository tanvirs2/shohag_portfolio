<?php include ("html_inc/header.php");?>
	<body class="layout-body layout-body--portfolio">
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="page-portfolio-single">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">My Project. <span class="layout-title__subtitle">In detail</span></h1>
						</div>
						<div class="layout-row"><img class="portfolio-single__image" src="uploads/portfolio/woman-in-glasses.jpg" alt=""></div>
						<div class="portfolio-single__info layout-horizontal-padding">
							<div>
								<div class="portfolio-single__info__name">Project name:</div>
								<div class="portfolio-single__info__description">Photoshoot for Vogue</div>
							</div>
							<div>
								<div class="portfolio-single__info__name">Completed:</div>
								<div class="portfolio-single__info__description">December 2014</div>
							</div>
							<div>
								<div class="portfolio-single__info__name">Client:</div>
								<div class="portfolio-single__info__description">Vogue</div>
							</div>
							<p class="portfolio-single__info__text">I couldn't love a woman who inspired me to be totally disinterested. If I fell in love with a woman for an artistic reason, or from the point of view of my work, I think it would rob her of something. Jeans represent democracy in fashion. Fashion never stops. There is always the new project, the new opportunity. Fashion can be this mysterious thing that you can't explain. I truly believe that philanthropy and commerce can work together.</p>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="portfolio-single.html#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="portfolio-single.html#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="portfolio-single.html#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="portfolio-single.html#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="portfolio-single.html#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="portfolio-single.html#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/portfolio-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:49 GMT -->
</html>
