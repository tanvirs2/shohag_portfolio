<?php include ("html_inc/header.php");
$portfolio_active = 1;
?>
	<body class="layout-body layout-body--portfolio">
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="page-portfolio">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">Portfolio. <span class="layout-title__subtitle">My photography</span></h1>
						</div>
						<div class="page-portfolio__items-container layout-row">
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/woman-in-glasses-small.jpg" alt="Autumn collection"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Autumn collection</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/lolipop-girl-small.jpg" alt="Wild sixties"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Wild sixties</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/bikini-small.jpg" alt="Sun beams"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Sun beams</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/girl-with-orange-small.jpg" alt="Faces"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Faces</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/smiling-woman-small.jpg" alt="Alice"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Alice</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/model-with-cameras-small.jpg" alt="Beauties"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Beauties</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/woman-in-glasses-small.jpg" alt="Style perfect"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Style perfect</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<div class="page-portfolio__item-wrapper">
								<div class="page-portfolio__item">
									<a href="portfolio-single.php"><img class="page-portfolio__image" src="uploads/portfolio/lolipop-girl-small.jpg" alt="Sweetie"></a>
									<div class="page-portfolio__item__info">
										<h2 class="page-portfolio__item__title">Sweetie</h2>
										<div class="page-portfolio__item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
										<a class="btn btn-lg btn--style2" href="portfolio-single.php">See More <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="portfolio.php#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="portfolio.php#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="portfolio.php#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="portfolio.php#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="portfolio.php#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="portfolio.php#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/portfolio.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:38 GMT -->
</html>
