<?php 
$index_active = 1;
require_once ('php_processor_file/app.php');

$query = "SELECT * FROM `home_page`";
$result = mysqli_query($link, $query);
$row = mysqli_fetch_assoc($result);
//var_dump($row);
//exit();
//echo $row['heading'];

include ("html_inc/header.php");?>
	<body class="layout-body layout-body--home">
            
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="page-home layout-row">
						<div class="row">
							<div class="col-md-6">
                                                            <h1 class="page-home__page-title"><?php echo $row['heading'];?></h1>
								<div class="page-home__text">
									<?php echo $row['description'];?>
								</div>
							</div>
							<div class="col-md-6 page-home__thumbnail-wrap"><img class="page-home__thumbnail" src="<?php echo $row['img'];?>" alt=""></div>
						</div>
						<div class="row">
							<div class="col-md-12"><img class="page-home__sign" src="<?php echo $row['sign'];?>" alt=""></div>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="index-2.html#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="index-2.html#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="index-2.html#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="index-2.html#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="index-2.html#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="index-2.html#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:17 GMT -->
</html>
