<?php include ("html_inc/header.php");
$contact_active = 1;
?>
	<body class="layout-body layout-body--contact">
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="page-contact">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">contact. <span class="layout-title__subtitle">Wanna talk to me?</span></h1>
						</div>
						
						<form action="http://oleg.dev/savvy/www/mail/fake.js" id="contactForm" method="post" class="layout-horizontal-padding">
							<div class="row">
								<div class="col-md-6">
									<input name="name" class="form__item" type="text" placeholder="name">
									<div class="form__item__failed-wrap"></div>
								</div>
								<div class="col-md-6">
									<input name="email" class="form__item" type="text" placeholder="email">
									<div class="form__item__failed-wrap"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<textarea name="msg" class="form__textarea form__item" placeholder="message"></textarea>
									<div class="form__item__failed-wrap"></div>
									<div class="form__status"></div>
									<button type="submit" class="btn btn-lg btn--style1">Send message</button>
								</div>
							</div>
						</form>
						<div class="layout-horizontal-padding--clean"></div>
						<div class="layout-horizontal-padding">
							<ul class="contact-info">
								<li><i class="fa fa-envelope"></i> <span>contact@rossi.com</span></li>
								<li><i class="fa fa-phone"></i> <span>(541) 754-3010</span></li>
								<li><i class="fa fa-map-marker"></i> <span>Santa Monica, CA</span></li>
							</ul>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="contact.html#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="contact.html#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="contact.html#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="contact.html#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="contact.html#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="contact.html#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:48 GMT -->
</html>
