<?php 
$resume_active = 1;
include ("html_inc/header.php");
require_once ('php_processor_file/app.php');
$query = "SELECT * FROM `experience`";
$result = mysqli_query($link, $query);

while ($row = mysqli_fetch_assoc($result)){
    $data[] = $row;
}
?>
	<body class="layout-body layout-body--resume">
            
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="page-resume">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">Resume. <span class="layout-title__subtitle">Up-to-date</span></h1>
						</div>
						<div class="layout-row">
							<div class="accordion-group" role="tablist" aria-multiselectable="true">
								<div class="accordion-item accordion-item--layout">
									<h2 class="accordion-title layout-container"><i class="accordion-title__icon fa fa-suitcase"></i>Experience </h2>
									<div id="collapse0" class="accordion-item__panel collapse in">
										<div class="timeline timeline--layout layout-container">
                                                                                    <?php if(isset($data) && count($data) > 0){
                                                                                        foreach ($data as $post){ ?>
                                                                                    <hr class="divider layout-row">
											<div class="timeline-item">
												<div class="timeline-item__col timeline-item__col--info">
													<div class="timeline-item__period"><?php echo $post['date'];?></div>
													<div class="timeline-item__place"><?php echo $post['writer_name'];?></div>
													<div class="timeline-item__location"><?php echo $post['place'];?></div>
                                                                                                        <img src="<?php echo $post['img'];?>" alt="">
												</div>
												<div class="timeline-item__col timeline-item__col--description">
													<div class="timeline-item__title"><?php echo $post['title'];?></div>
													<div class="timeline-item__description"><?php echo $post['description'];?></div>
												</div>
											</div>
                                                                                    <?php }}  else {?>
											<div class="timeline-item">
												<div class="timeline-item__col timeline-item__col--info">
													<div class="timeline-item__period"></div>
													<div class="timeline-item__place"></div>
													<div class="timeline-item__location"></div>
                                                                                                        <img src="uploads/resume/coming.png" alt="">
												</div>
												<div class="timeline-item__col timeline-item__col--description">
													<div class="timeline-item__title">Coming Soon...!</div>
													<div class="timeline-item__description"></div>
												</div>
											</div>
                                                                                    <?php } ?>
										</div>
									</div>
								</div>
								<div class="accordion-item accordion-item--layout">
									<h2 class="accordion-title layout-container collapsed"><i class="accordion-title__icon fa fa-cubes"></i>Skills </h2>
									<div id="collapse0" class="accordion-item__panel collapse in">
										<div class="layout-container">
											<div class="skill-section">
												<h3 class="skill-section__title">IT/Programming</h3>
														<ul class="list-style--check list-style--check--resume">
															<li>knowledge of digital photography</li>
															<li>knowledge of flash photography equipment</li>
															<li>strong organizational skills</li>
															<li>excellent interpersonal skills</li>
														</ul>
											<div class="skill-section">
												<h3 class="skill-section__title">Automtion Engineering</h3>
														<ul class="list-style--check list-style--check--resume">
															<li>knowledge of digital photography</li>
															<li>knowledge of flash photography equipment</li>
															<li>strong organizational skills</li>
															<li>excellent interpersonal skills</li>
														</ul>
												
											</div>
											<hr class="divider layout-row">
											<div class="skill-section">
												<h3 class="skill-section__title">Languages</h3>
												<div class="rating">
													<div class="rating__title">Bangali</div>
													<div data-score="8" class="rating__score"></div>
													<div class="rating__value">Native</div>
												</div>
												<div class="rating">
													<div class="rating__title">English</div>
													<div data-score="6" class="rating__score"></div>
													<div class="rating__value">Fluent</div>
												</div>
												
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-item accordion-item--layout">
									<h2 class="accordion-title layout-container collapsed"><i class="accordion-title__icon fa fa-graduation-cap"></i>Education </h2>
									<div id="collapse2" class="accordion-item__panel collapse in">
										<div class="timeline timeline--layout layout-container">
											<div class="timeline-item">
												<div class="timeline-item__col timeline-item__col--info">
													<div class="timeline-item__period">2014 - 2018</div>
													<div class="timeline-item__place">IUBAT-International University of Business Agriculture and Technology</div>
													<div class="timeline-item__location">Dhaka, Bangladesh</div>
													<img src="uploads/resume/iubat-logo.jpg" alt="">
												</div>
												<div class="timeline-item__col timeline-item__col--description">
													<div class="timeline-item__title">BSc in Electrical and Electronic Engineering</div>
													<div class="timeline-item__description">
														<ul class="list-style--check">
															<li>Project management overview</li>
															<li>Project management methodology</li>
															<li>Project management toolset</li>
															<li>Project management documentation</li>
															<li>System development life cycle</li>
														</ul>
													</div>
												</div>
											</div>
											<hr class="divider layout-row">
											<div class="timeline-item">
												<div class="timeline-item__col timeline-item__col--info">
													<div class="timeline-item__period">2009 - 2013</div>
													<div class="timeline-item__place">Dhaka Polytechnic Institute</div>
													<div class="timeline-item__location">Dhaka Bangladesh</div>
													<img src="uploads/resume/dpi-logo.jpg" alt="">
												</div>
												<div class="timeline-item__col timeline-item__col--description">
													<div class="timeline-item__title">Diploma in Electronic Engineering</div>
													<div class="timeline-item__description">
														<ul class="list-style--check">
															<li>Programming for Computer Scientists</li>
															<li>Design of Information Structures</li>
															<li>Mathematics for Computer Scientists</li>
															<li>Computer Organisation and Architecture</li>
															<li>Professional Skills</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="accordion-item accordion-item--layout">
									<h2 class="accordion-title layout-container collapsed"><i class="accordion-title__icon fa fa-graduation-cap"></i>Training </h2>
									<div id="collapse2" class="accordion-item__panel collapse in">
										<div class="timeline timeline--layout layout-container">
											<div class="timeline-item">
												<div class="timeline-item__col timeline-item__col--info">
													<div class="timeline-item__period">2015</div>
													<div class="timeline-item__place">BASIS Institute of Technology and Management (BITM)</div>
													<div class="timeline-item__location">Dhaka, Bangladesh</div>
													<img src="uploads/resume/bitm-logo.jpg" alt="">
												</div>
												<div class="timeline-item__col timeline-item__col--description">
													<div class="timeline-item__title">Mobile Application Development (Android)</div>
													
														<ul class="list-style--check">
															<li>Project management overview</li>
															<li>Project management methodology</li>
															<li>Project management toolset</li>
															<li>Project management documentation</li>
															<li>System development life cycle</li>
														</ul>
													
												</div>
											</div>
											<hr class="divider layout-row">
											<div class="timeline-item">
												<div class="timeline-item__col timeline-item__col--info">
													<div class="timeline-item__period">2013</div>
													<div class="timeline-item__place">Rainbow Automation</div>
													<div class="timeline-item__location">Dhaka, Bangladesh</div>
													<img src="uploads/resume/rainbow-logo.jpg" alt="">
												</div>
												<div class="timeline-item__col timeline-item__col--description">
													<div class="timeline-item__title">National Skill Standard Basic (360 hrs)</div>
													<div class="timeline-item__description">
														<ul class="list-style--check">
															<li>Programming for Computer Scientists</li>
															<li>Design of Information Structures</li>
															<li>Mathematics for Computer Scientists</li>
															<li>Computer Organisation and Architecture</li>
															<li>Professional Skills</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="resume.html#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="resume.html#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="resume.html#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="resume.html#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="resume.html#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="resume.html#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
                <script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
                
	</body>
	<!-- Mirrored from savvy.themedelight.com/resume.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:23 GMT -->
</html>
