<?php include ("html_inc/header.php");
$blog_active = 1;
?>
	<body class="layout-body layout-body--blog">
		<?php include ("html_inc/menu.php");?>
			<div class="layout-container-main layout-container-width">
				<div class="layout-content">
					<section class="blog">
						<div class="layout-title-wrapper layout-row">
							<h1 class="layout-title layout-container-width layout-container">Blog. <span class="layout-title__subtitle">About everything</span></h1>
						</div>
						<div class="blog-posts">
							<div class="blog-post layout-row">
								<a class="blog-post__image-wrap" href="blog-single.html"><img class="blog-post__image" src="uploads/blog/desktop.jpg" alt=""></a>
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">How Workspace Influences Productivity</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>20 December 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>work</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>work, office</a></li>
										<li><a href="blog.html#"><i class="fa fa-comments-o"></i>14 comments</a></li>
									</ul>
									<p class="blog-post__content">Narwhal cornhole pop-up twee fixie. Normcore 90's quinoa ethical cardigan, crucifix gluten-free food truck Thundercats mustache. XOXO dreamcatcher beard, readymade hella messenger bag artisan Tumblr Helvetica Vice Pitchfork kogi health goth bitters tilde. Aesthetic taxidermy vegan paleo trust fund cliche. Viral bespoke Pinterest, mixtape Echo Park drinking vinegar fingerstache Marfa squid. Selfies selvage organic umami fashion axe listicle. Trust fund swag health goth, pop-up tousled pour-over 3 wolf moon mlkshk Vice small batch.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
							<div class="blog-post layout-row">
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">This Is a Post without Image</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>15 December 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>life</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>love, fun</a></li>
									</ul>
									<p class="blog-post__content">Tumblr selfies Bushwick, mumblecore meditation kale chips semiotics tofu tousled High Life. IPhone McSweeney's Carles Williamsburg. Lomo High Life sartorial, cred tattooed occupy sustainable chambray XOXO PBR ennui taxidermy try-hard gastropub irony. Pitchfork pug crucifix, iPhone Truffaut bespoke Bushwick cred selvage Kickstarter banjo kogi retro scenester squid.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
							<div class="blog-post layout-row">
								<a class="blog-post__image-wrap" href="blog-single.html"><img class="blog-post__image" src="uploads/blog/autumn-cafe.jpg" alt=""></a>
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">Autumn in New York City</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>26 April 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>life, travelling</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>autumn, New York</a></li>
									</ul>
									<p class="blog-post__content">PBR&amp;B artisan flexitarian, organic small batch blog art party Neutra Pitchfork High Life. Literally 3 wolf moon McSweeney's tofu Shoreditch High Life. Seitan ennui paleo, ethical put a bird on it Portland single-origin coffee Helvetica gentrify banjo dreamcatcher blog. Intelligentsia retro sartorial, shabby chic messenger bag dreamcatcher Brooklyn PBR biodiesel artisan Schlitz typewriter YOLO listicle Tumblr.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
							<div class="blog-post layout-row">
								<a class="blog-post__image-wrap" href="blog-single.html"><img class="blog-post__image" src="uploads/blog/dried-mandarins.jpg" alt=""></a>
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">Homemade Dried Mandarin Peels</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>23 April 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>cooking</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>mandarins, homemade</a></li>
										<li><a href="blog.html#"><i class="fa fa-comments-o"></i>41 comments</a></li>
									</ul>
									<p class="blog-post__content">Blue Bottle lo-fi chambray scenester Vice tote bag. Migas fap flannel, semiotics slow-carb Marfa retro tofu shabby chic skateboard swag. Bespoke hella mlkshk, flannel slow-carb Pinterest gluten-free fingerstache cardigan pour-over deep v. Organic street art scenester Williamsburg bitters. Banksy letterpress gastropub, selfies banjo meditation wayfarers Odd Future hoodie Intelligentsia cardigan. Craft beer kogi Williamsburg, ethical selfies cold-pressed crucifix messenger bag scenester put a bird on it aesthetic twee. Mustache leggings readymade migas cold-pressed.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
							<div class="blog-post layout-row">
								<a class="blog-post__image-wrap" href="blog-single.html"><img class="blog-post__image" src="uploads/blog/book-reading.jpg" alt=""></a>
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">Do You Still Read Paper Books?</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>18 April 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>pastime</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>paper books, reading</a></li>
										<li><a href="blog.html#"><i class="fa fa-comments-o"></i>18 comments</a></li>
									</ul>
									<p class="blog-post__content">Tilde church-key Vice, tattooed farm-to-table pour-over letterpress flexitarian High Life 8-bit DIY. Migas hoodie irony mustache 90's stumptown chambray. Pinterest whatever gentrify, banh mi lomo cred disrupt. Mumblecore roof party flexitarian Carles, sriracha fixie meggings typewriter small batch Marfa single-origin coffee swag post-ironic McSweeney's tilde. Tilde mustache pop-up paleo meggings, cray retro occupy blog ethical listicle letterpress.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
							<div class="blog-post layout-row">
								<a class="blog-post__image-wrap" href="blog-single.html"><img class="blog-post__image" src="uploads/blog/drinking-tea.jpg" alt=""></a>
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">Tea Drinking Tradition in England</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>29 March 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>travelling, cooking</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>tea, traditions, England</a></li>
										<li><a href="blog.html#"><i class="fa fa-comments-o"></i>39 comments</a></li>
									</ul>
									<p class="blog-post__content">Single-origin coffee semiotics fashion axe gastropub, distillery readymade mustache blog meh put a bird on it kitsch gentrify YOLO swag. Raw denim leggings umami typewriter keytar. Hoodie Godard keytar Echo Park Shoreditch, you probably haven't heard of them drinking vinegar raw denim twee sustainable leggings meditation. Truffaut chia Echo Park bicycle rights, beard mustache tote bag before they sold out ennui 90's YOLO flannel.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
							<div class="blog-post layout-row">
								<a class="blog-post__image-wrap" href="blog-single.html"><img class="blog-post__image" src="uploads/blog/office-table.jpg" alt=""></a>
								<div class="layout-container layout-horizontal-padding">
									<h2 class="blog-post__title"><a href="blog-single.html">Benefits of Being Well-Organized</a></h2>
									<ul class="blog-post__article-info">
										<li><a href="blog.html#"><i class="fa fa-calendar"></i>26 March 2014</a></li>
										<li><a href="blog.html#"><i class="fa fa-folder-open"></i>work</a></li>
										<li><a href="blog.html#"><i class="fa fa-tags"></i>work, organization, desktop</a></li>
										<li><a href="blog.html#"><i class="fa fa-comments-o"></i>27 comments</a></li>
									</ul>
									<p class="blog-post__content">Literally scenester flannel, Odd Future semiotics gastropub biodiesel put a bird on it ennui post-ironic skateboard pop-up banjo. Messenger bag fap Tumblr 90's, scenester Williamsburg pickled chia lumbersexual slow-carb ennui Pitchfork. Health goth farm-to-table forage, +1 VHS Odd Future swag aesthetic trust fund Carles Blue Bottle PBR&amp;B artisan scenester.</p>
									<a class="btn btn-lg btn--style1 btn--blog" href="blog-single.html">Read More</a>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="footer--bottom layout-row">
					<p class="footer__copyright">&copy; Rossi, 2015. All rights reserved.</p>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><script type="text/javascript" src="assets/jslib/jquery-1.11.2.js"></script><script type="text/javascript" src="assets/js/basic-full.js"></script><script type="text/javascript" src="assets/js/Template.js"></script>
		<div class="style-panel" style="display:none">
			<div class="style-panel__button"><i class="fa fa-paint-brush"></i></div>
			<div class="style-panel__title">Choose a color:</div>
			<div class="style-panel__colors"><a data-value="style1" href="blog.html#" class="style-panel__color style-panel__color--style1"></a> <a data-value="style2" href="blog.html#" class="style-panel__color style-panel__color--style2"></a> <a data-value="style3" href="blog.html#" class="style-panel__color style-panel__color--style3"></a> <a data-value="style4" href="blog.html#" class="style-panel__color style-panel__color--style4"></a> <a data-value="style5" href="blog.html#" class="style-panel__color style-panel__color--style5"></a> <a data-value="style6" href="blog.html#" class="style-panel__color style-panel__color--style6"></a></div>
			<div class="style-panel__info">and many more...</div>
			<div class="style-panel__reset"><button class="btn-reset btn btn-xs btn--style1">reset</button></div>
		</div>
		<script type="text/javascript" src="assets/jslib/jquery.cookie.js"></script><script type="text/javascript" src="assets/js/StylePanel.js"></script>
	</body>
	<!-- Mirrored from savvy.themedelight.com/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Nov 2015 19:07:48 GMT -->
</html>
